package com.escoffier.myexternship;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeSheet extends AppCompatActivity implements View.OnTouchListener {

    private GestureDetectorCompat mDetector;
    private ScrollView svTimeSheet;
    private TimeSheetData mTsData;
    private Date mDateShown;
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    private EditText txtSupervisorFeedback;
    private EditText txtSupervisorName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_sheet);

        mDetector = new GestureDetectorCompat(this, new MyGestureDetector());

        svTimeSheet = (ScrollView) findViewById(R.id.svTimeSheet);
        svTimeSheet.setOnTouchListener(this);

        ContextWrapper cw = new ContextWrapper(this);
        mTsData = new TimeSheetData(cw);
        Date startDate = mTsData.getCurrentWeek();
        mDateShown = startDate;
        TextView lblStartDate = (TextView) findViewById(R.id.lblStartDate);
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.CANADA);
        lblStartDate.setText("Starting "+ df.format((startDate)));

        ratingBarChangeListener();
        supervisorChangeListener();
        signatureListener();
        loadTimeSheet();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return  super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        onTouchEvent(event);

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mnuSettings:
                handleSettings();
                return true;
            case R.id.mnuTimer:
                handleTimer();
                return true;
            case R.id.mnuTimeSheet:
                handleTimeSheet();
                return true;
            case R.id.mnuLogOff:
                handleLogOff();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void signatureListener(){
        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        mClearButton = (Button) findViewById(R.id.clear_button);
        mSaveButton = (Button) findViewById(R.id.save_button);

        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                //Event triggered when the pad is signed
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                //Event triggered when the pad is cleared
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);

            }
        });

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
                ImageView iv = (ImageView) findViewById(R.id.ivSignature);
                if(iv.getVisibility() == View.VISIBLE){
                    iv.setVisibility(View.GONE);
                    mSignaturePad.setVisibility(View.VISIBLE);
                }
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                //Convert bitmap to text
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                signatureBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] bytes = baos.toByteArray();
                String base64String = Base64.encodeToString(bytes, Base64.DEFAULT);
                String dt = mTsData.getDataForDay(mDateShown, "mon");
                mTsData.setWeekData(dt, "signature", base64String);
                mSaveButton.setEnabled(false);
                Toast.makeText(TimeSheet.this, "Saved Week Data", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void supervisorChangeListener(){

        txtSupervisorFeedback = (EditText) findViewById(R.id.txtSupervisorFeedback);
        txtSupervisorFeedback.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                String dt = mTsData.getDataForDay(mDateShown, "mon");
                mTsData.setWeekData(dt, "txtSupervisorFeedback", s.toString());
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

        });


        txtSupervisorName = (EditText) findViewById(R.id.txtSupervisorName);
        txtSupervisorName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                String dt = mTsData.getDataForDay(mDateShown, "mon");
                mTsData.setWeekData(dt, "txtSupervisorName", s.toString());
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

        });
    }

    private void ratingBarChangeListener(){


        RatingBar.OnRatingBarChangeListener rbListener = new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                String dt = mTsData.getDataForDay(mDateShown, "mon");

                switch (ratingBar.getId()){
                    case R.id.rbCookingTechniques:
                        mTsData.setWeekData(dt, "rbCookingTechniques", String.valueOf(rating));
                        break;
                    case R.id.rbEquipmentSkills:
                        mTsData.setWeekData(dt, "rbEquipmentSkills", String.valueOf(rating));
                        break;
                    case R.id.rbKnifeSkills:
                        mTsData.setWeekData(dt, "rbKnifeSkills", String.valueOf(rating));
                        break;
                    case R.id.rbProfessionalism:
                        mTsData.setWeekData(dt, "rbProfessionalism", String.valueOf(rating));
                        break;
                    case R.id.rbReliability:
                        mTsData.setWeekData(dt, "rbReliability", String.valueOf(rating));
                        break;
                    case R.id.rbSanitation:
                        mTsData.setWeekData(dt, "rbSanitation", String.valueOf(rating));
                        break;
                    case R.id.rbSociability:
                        mTsData.setWeekData(dt, "rbSociability", String.valueOf(rating));
                        break;
                    case R.id.rbTerminology:
                        mTsData.setWeekData(dt, "rbTerminology", String.valueOf(rating));
                        break;
                    case R.id.rbTimeManagement:
                        mTsData.setWeekData(dt, "rbTimeManagement", String.valueOf(rating));
                        break;
                    case R.id.rbWorkEthic:
                        mTsData.setWeekData(dt, "rbWorkEthic", String.valueOf(rating));
                        break;
                }

            }
        };
        RatingBar rbCookingTechniques = (RatingBar) findViewById(R.id.rbCookingTechniques);
        rbCookingTechniques.setOnRatingBarChangeListener(rbListener);

        RatingBar rbEquipmentSkills = (RatingBar) findViewById(R.id.rbEquipmentSkills);
        rbEquipmentSkills.setOnRatingBarChangeListener(rbListener);

        RatingBar rbKnifeSkills = (RatingBar) findViewById(R.id.rbKnifeSkills);
        rbKnifeSkills.setOnRatingBarChangeListener(rbListener);

        RatingBar rbProfessionalism = (RatingBar) findViewById(R.id.rbProfessionalism);
        rbProfessionalism.setOnRatingBarChangeListener(rbListener);

        RatingBar rbReliability = (RatingBar) findViewById(R.id.rbReliability);
        rbReliability.setOnRatingBarChangeListener(rbListener);

        RatingBar rbSanitation = (RatingBar) findViewById(R.id.rbSanitation);
        rbSanitation.setOnRatingBarChangeListener(rbListener);

        RatingBar rbSociability = (RatingBar) findViewById(R.id.rbSociability);
        rbSociability.setOnRatingBarChangeListener(rbListener);

        RatingBar rbTerminology = (RatingBar) findViewById(R.id.rbTerminology);
        rbTerminology.setOnRatingBarChangeListener(rbListener);

        RatingBar rbTimeManagement = (RatingBar) findViewById(R.id.rbTimeManagement);
        rbTimeManagement.setOnRatingBarChangeListener(rbListener);

        RatingBar rbWorkEthic = (RatingBar) findViewById(R.id.rbWorkEthic);
        rbWorkEthic.setOnRatingBarChangeListener(rbListener);

    }

    private void handleLogOff() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("email", "");
        editor.apply();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        finish();//we no longer need login
    }

    private void handleTimeSheet() {

    }

    private void handleTimer() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        long startTime =  sharedPref.getLong("timerStart", 0);

        Intent intent;

        if(startTime > 0){
            intent = new Intent(this, TrackTimeStop.class);
        }else{
            intent = new Intent(this, TrackTime.class);
        }

        startActivity(intent);
    }

    private void handleSettings() {
        Intent intent = new Intent(this, StudentProfile.class);
        startActivity(intent);
    }

    private void onRightSwipe(){
        Date startDate = mTsData.getPreviousWeek(mDateShown);
        mDateShown = startDate;
        TextView lblStartDate = (TextView) findViewById(R.id.lblStartDate);
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.CANADA);
        lblStartDate.setText("Starting "+ df.format((startDate)));
        loadTimeSheet();
    }

    private void onLeftSwipe(){
        Date startDate = mTsData.getNextWeek(mDateShown);

        if(startDate.getTime() > mTsData.getCurrentWeek().getTime()){
            Toast.makeText(this, "Already on Present Week", Toast.LENGTH_LONG).show();
        }else {
            mDateShown = startDate;
            TextView lblStartDate = (TextView) findViewById(R.id.lblStartDate);
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.CANADA);
            lblStartDate.setText("Starting " + df.format((startDate)));
            loadTimeSheet();
        }

    }

    /**
     * Loads timesheet with saved data
     */
    private void loadTimeSheet(){
        String dt;
        double hrs;
        TextView lbl;
        String tm;


        dt = mTsData.getDataForDay(mDateShown, "mon");
        hrs = Double.parseDouble(mTsData.getDateData(dt));
        tm = getStringFromHrs(hrs);
        lbl = (TextView)findViewById(R.id.lblMondayHours);
        lbl.setText(tm);

        dt = mTsData.getDataForDay(mDateShown, "tue");
        hrs = Double.parseDouble(mTsData.getDateData(dt));
        tm = getStringFromHrs(hrs);
        lbl = (TextView)findViewById(R.id.lblTuesdayHours);
        lbl.setText(tm);

        dt = mTsData.getDataForDay(mDateShown, "wed");
        hrs = Double.parseDouble(mTsData.getDateData(dt));
        tm = getStringFromHrs(hrs);
        lbl = (TextView)findViewById(R.id.lblWednesdayHours);
        lbl.setText(tm);

        dt = mTsData.getDataForDay(mDateShown, "thu");
        hrs = Double.parseDouble(mTsData.getDateData(dt));
        tm = getStringFromHrs(hrs);
        lbl = (TextView)findViewById(R.id.lblThursdayHours);
        lbl.setText(tm);

        dt = mTsData.getDataForDay(mDateShown, "fri");
        hrs = Double.parseDouble(mTsData.getDateData(dt));
        tm = getStringFromHrs(hrs);
        lbl = (TextView)findViewById(R.id.lblFridayHours);
        lbl.setText(tm);

        dt = mTsData.getDataForDay(mDateShown, "sat");
        hrs = Double.parseDouble(mTsData.getDateData(dt));
        tm = getStringFromHrs(hrs);
        lbl = (TextView)findViewById(R.id.lblSaturdayHours);
        lbl.setText(tm);

        dt = mTsData.getDataForDay(mDateShown, "sun");
        hrs = Double.parseDouble(mTsData.getDateData(dt));
        tm = getStringFromHrs(hrs);
        lbl = (TextView)findViewById(R.id.lblSundayHours);
        lbl.setText(tm);

        RatingBar rbCookingTechniques = (RatingBar) findViewById(R.id.rbCookingTechniques);
        dt = mTsData.getDataForDay(mDateShown, "mon");
        tm = mTsData.getWeekData(dt, "rbCookingTechniques");
        rbCookingTechniques.setRating(Float.valueOf(tm));

        RatingBar rbEquipmentSkills = (RatingBar) findViewById(R.id.rbEquipmentSkills);
        tm = mTsData.getWeekData(dt, "rbEquipmentSkills");
        rbEquipmentSkills.setRating(Float.valueOf(tm));

        RatingBar rbKnifeSkills = (RatingBar) findViewById(R.id.rbKnifeSkills);
        tm = mTsData.getWeekData(dt, "rbKnifeSkills");
        rbKnifeSkills.setRating(Float.valueOf(tm));

        RatingBar rbProfessionalism = (RatingBar) findViewById(R.id.rbProfessionalism);
        tm = mTsData.getWeekData(dt, "rbProfessionalism");
        rbProfessionalism.setRating(Float.valueOf(tm));

        RatingBar rbReliability = (RatingBar) findViewById(R.id.rbReliability);
        tm = mTsData.getWeekData(dt, "rbReliability");
        rbReliability.setRating(Float.valueOf(tm));

        RatingBar rbSanitation = (RatingBar) findViewById(R.id.rbSanitation);
        tm = mTsData.getWeekData(dt, "rbSanitation");
        rbSanitation.setRating(Float.valueOf(tm));

        RatingBar rbSociability = (RatingBar) findViewById(R.id.rbSociability);
        tm = mTsData.getWeekData(dt, "rbSociability");
        rbSociability.setRating(Float.valueOf(tm));

        RatingBar rbTerminology = (RatingBar) findViewById(R.id.rbTerminology);
        tm = mTsData.getWeekData(dt, "rbTerminology");
        rbTerminology.setRating(Float.valueOf(tm));

        RatingBar rbTimeManagement = (RatingBar) findViewById(R.id.rbTimeManagement);
        tm = mTsData.getWeekData(dt, "rbTimeManagement");
        rbTimeManagement.setRating(Float.valueOf(tm));

        RatingBar rbWorkEthic = (RatingBar) findViewById(R.id.rbWorkEthic);
        tm = mTsData.getWeekData(dt, "rbWorkEthic");
        rbWorkEthic.setRating(Float.valueOf(tm));

        EditText txtSupervisorFeedback = (EditText) findViewById(R.id.txtSupervisorFeedback);
        tm = mTsData.getWeekData(dt, "txtSupervisorFeedback");
        txtSupervisorFeedback.setText(tm);

        EditText txtSupervisorName = (EditText) findViewById(R.id.txtSupervisorName);
        tm = mTsData.getWeekData(dt, "txtSupervisorName");
        txtSupervisorName.setText(tm);

        tm = mTsData.getWeekData(dt, "signature");
        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        ImageView iv = (ImageView) findViewById(R.id.ivSignature);
        if(tm.equals("0")){
            iv.setVisibility(View.GONE);
            mSignaturePad.setVisibility(View.VISIBLE);
            mClearButton.setEnabled(true);
            mSaveButton.setEnabled(true);
        }else{
            iv.setVisibility(View.VISIBLE);
            mSignaturePad.setVisibility(View.GONE);
            mClearButton.setEnabled(true);
            mSaveButton.setEnabled(false);

            byte[] decodedString = Base64.decode(tm, Base64.DEFAULT);
            Bitmap bitMap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            iv.setImageBitmap(bitMap);
        }
        showWeekTotalHours();

    }

    private void showWeekTotalHours(){
        String tm;
        TextView lbl;

        tm = mTsData.getTotalHours(mDateShown);
        lbl = (TextView)findViewById(R.id.lblTotalHours);
        lbl.setText(tm);

    }

    private String getStringFromHrs(double hrs){
        double totalMinutes = hrs * 60;
        int hr = (int)totalMinutes/60;

        int minutes = (int) Math.round(totalMinutes % 60);
        minutes = minutes < 0? minutes + 60 : minutes;

        return String.format("%02d:%02d", hr, minutes);
    }

    public void btnPlusMinusClicked(View view) {
        String dt;
        double hrs;
        double step = 10.0/60.0;
        TextView lbl;
        String tm;

        switch(view.getId()) {
            case R.id.btnMondayMinus:
                dt = mTsData.getDataForDay(mDateShown, "mon");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                if(hrs == 0) return;
                hrs = hrs - step;
                if(hrs < 0) hrs = 0;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblMondayHours);
                lbl.setText(tm);
                break;
            case R.id.btnMondayPlus:
                dt = mTsData.getDataForDay(mDateShown, "mon");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                hrs = hrs + step;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblMondayHours);
                lbl.setText(tm);
                break;

            case R.id.btnTuesdayMinus:
                dt = mTsData.getDataForDay(mDateShown, "tue");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                if(hrs == 0) return;
                hrs = hrs - step;
                if(hrs < 0) hrs = 0;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblTuesday);
                lbl.setText(tm);
                break;
            case R.id.btnTuesdayPlus:
                dt = mTsData.getDataForDay(mDateShown, "tue");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                hrs = hrs + step;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblTuesdayHours);
                lbl.setText(tm);
                break;

            case R.id.btnWednesdayMinus:
                dt = mTsData.getDataForDay(mDateShown, "wed");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                if(hrs == 0) return;
                hrs = hrs - step;
                if(hrs < 0) hrs = 0;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblWednesdayHours);
                lbl.setText(tm);
                break;
            case R.id.btnWednesdayPlus:
                dt = mTsData.getDataForDay(mDateShown, "wed");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                hrs = hrs + step;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblWednesdayHours);
                lbl.setText(tm);
                break;

            case R.id.btnThursdayMinus:
                dt = mTsData.getDataForDay(mDateShown, "thu");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                if(hrs == 0) return;
                hrs = hrs - step;
                if(hrs < 0) hrs = 0;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblThursdayHours);
                lbl.setText(tm);
                break;
            case R.id.btnThursdayPlus:
                dt = mTsData.getDataForDay(mDateShown, "thu");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                hrs = hrs + step;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblThursdayHours);
                lbl.setText(tm);
                break;

            case R.id.btnFridayMinus:
                dt = mTsData.getDataForDay(mDateShown, "fri");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                if(hrs == 0) return;
                hrs = hrs - step;
                if(hrs < 0) hrs = 0;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblFridayHours);
                lbl.setText(tm);
                break;
            case R.id.btnFridayPlus:
                dt = mTsData.getDataForDay(mDateShown, "fri");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                hrs = hrs + step;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblFridayHours);
                lbl.setText(tm);
                break;

            case R.id.btnSaturdayMinus:
                dt = mTsData.getDataForDay(mDateShown, "sat");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                if(hrs == 0) return;
                hrs = hrs - step;
                if(hrs < 0) hrs = 0;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblSaturdayHours);
                lbl.setText(tm);
                break;
            case R.id.btnSaturdayPlus:
                dt = mTsData.getDataForDay(mDateShown, "sat");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                hrs = hrs + step;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblSaturdayHours);
                lbl.setText(tm);
                break;

            case R.id.btnSundayMinus:
                dt = mTsData.getDataForDay(mDateShown, "sun");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                if(hrs == 0) return;
                hrs = hrs - step;
                if(hrs < 0) hrs = 0;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblSundayHours);
                lbl.setText(tm);
                break;
            case R.id.btnSundayPlus:
                dt = mTsData.getDataForDay(mDateShown, "sun");
                hrs = Double.parseDouble(mTsData.getDateData(dt));
                hrs = hrs + step;

                mTsData.setDateData(dt, String.valueOf(hrs));
                tm = getStringFromHrs(hrs);
                lbl = (TextView)findViewById(R.id.lblSundayHours);
                lbl.setText(tm);
                break;
        }
        showWeekTotalHours();

    }

    //inner class for gesture
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH){
                    return false;
                }
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    onLeftSwipe();
                }
                // left to right swipe
                else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    onRightSwipe();
                }
            } catch (Exception e) {
                int x=0;
            }
            return false;
        }
    }


}
