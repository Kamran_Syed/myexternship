package com.escoffier.myexternship;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class StudentProfile extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_profile);

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String email =  sharedPref.getString("email", "");

        if(email.equals("")) {
            //finish();   //must be logged in
        }

        String name =  sharedPref.getString("studentName", "");
        String campus =  sharedPref.getString("campus", "");
        String phone =  sharedPref.getString("profilePhone", "");

        //EditText studentPhone = (EditText) findViewById(R.id.txtStudentPhone);
        //EditText studentName = (EditText) findViewById(R.id.txtStudentName);
        Spinner studentCampus = (Spinner) findViewById(R.id.spCampus);
        studentCampus.setOnItemSelectedListener(this);

        if(phone.equals("")){
            TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String getSimSerialNumber = telemamanger.getSimSerialNumber();
            String getSimNumber = telemamanger.getLine1Number();
            //if(getSimNumber != null) profilePhone.setText(getSimNumber);
        }else{
            //studentName.setText(name);
            //studentName.setText(phone);
        }

        //fill spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.arrCampus, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        studentCampus.setAdapter(adapter);

/*        if(! campus.equals("")){
            //profileCampus.setSelection(adapter.getPosition(campus));
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return  super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mnuSettings:
                handleSettings();
                return true;
            case R.id.mnuTimer:
                handleTimer();
                return true;
            case R.id.mnuTimeSheet:
                handleTimeSheet();
                return true;
            case R.id.mnuLogOff:
                handleLogOff();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void handleLogOff() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("email", "");
        editor.apply();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        finish();//we no longer need login
    }

    private void handleTimeSheet() {
        Intent intent = new Intent(this, TimeSheet.class);
        startActivity(intent);
    }

    private void handleTimer() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        long startTime =  sharedPref.getLong("timerStart", 0);

        Intent intent;

        if(startTime > 0){
            intent = new Intent(this, TrackTimeStop.class);
        }else{
            intent = new Intent(this, TrackTime.class);
        }

        startActivity(intent);
    }

    private void handleSettings() {

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void handleSaveProfile(View view) {
        Toast.makeText(this, "Profile Saved", Toast.LENGTH_LONG).show();
    }


}
