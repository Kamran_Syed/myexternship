package com.escoffier.myexternship;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;

public class TrackTimeStop extends AppCompatActivity {
    TimeSheetData mTsData;
    Date mDateShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_time_stop);

        ContextWrapper cw = new ContextWrapper(this);
        mTsData = new TimeSheetData(cw);
        mDateShown = mTsData.getCurrentWeek();

        String dt = mTsData.getDataForDay(mDateShown, "today");
        double hrs = Double.parseDouble(mTsData.getDateData(dt));
        String tm = mTsData.getStringFromHrs(hrs);
        TextView lblHoursToday = (TextView)findViewById(R.id.lblHoursToday);
        lblHoursToday.setText(tm);

        tm = mTsData.getTotalHours(mDateShown);
        TextView lblHoursWeek = (TextView)findViewById(R.id.lblHoursWeek);
        lblHoursWeek.setText(tm);

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String timerStartedAt =  sharedPref.getString("timerStartedAt", "09:00 am");
        TextView lblTimer = (TextView)findViewById(R.id.lblTimer);
        lblTimer.setText("Timer Started AT "+timerStartedAt);
    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return  super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mnuSettings:
                handleSettings();
                return true;
            case R.id.mnuTimer:
                handleTimer();
                return true;
            case R.id.mnuTimeSheet:
                handleTimeSheet();
                return true;
            case R.id.mnuLogOff:
                handleLogOff();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void handleLogOff() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("email", "");
        editor.apply();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        finish();//we no longer need login
    }

    private void handleTimeSheet() {
        Intent intent = new Intent(this, TimeSheet.class);
        startActivity(intent);
    }

    private void handleTimer() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        long startTime =  sharedPref.getLong("timerStart", 0);

        Intent intent;

        if(startTime > 0){
            intent = new Intent(this, TrackTimeStop.class);
        }else{
            intent = new Intent(this, TrackTime.class);
        }

        startActivity(intent);
    }

    private void handleSettings() {
        /*Intent intent = new Intent(this, MyProfile.class);
        startActivity(intent);*/
        //
    }

    private void updateTimer(boolean pause, boolean stop){
        SharedPreferences sharedPref = getSharedPreferences("com.escoffier.myexternship.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
        long startTime =  sharedPref.getLong("timerStart", 0);

        if(pause && !stop){
            double hrs = (double) (mTsData.getTime() - startTime)/60000 ;
            String dt = mTsData.getDataForDay(mDateShown, "today");
            double oldhrs = Double.parseDouble(mTsData.getDateData(dt));
            mTsData.setDateData(dt, String.valueOf(hrs + oldhrs));

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putLong("timerStart", 0);
            editor.putString("timerStartedAt", "");
            editor.apply();

        }else if(! pause && !stop){
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putLong("timerStart", mTsData.getTime());
            editor.putString("timerStartedAt", mTsData.getFormatedTime());
            editor.apply();

        }else if(stop){
            if(startTime ==0) return; int x=0;
            double hrs = (double) (mTsData.getTime() - startTime)/60000 ;
            String dt = mTsData.getDataForDay(mDateShown, "today");
            double oldhrs = Double.parseDouble(mTsData.getDateData(dt));
            mTsData.setDateData(dt, String.valueOf(hrs + oldhrs));

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putLong("timerStart", 0);
            editor.putString("timerStartedAt", "");
            editor.apply();
        }
    }

    public void btnPauseClicked(View view) {
        Button btn = (Button)findViewById(R.id.btnPause);
        String txt = btn.getText().toString();
        TextView lblTimer = (TextView)findViewById(R.id.lblTimer);
        String timerStartedAt = mTsData.getFormatedTime();

        if(txt.equals("PAUSE")){
            btn.setText("RESUME");
            lblTimer.setText("Timer Paused AT "+timerStartedAt);
            updateTimer(true, false);

        }else{
            btn.setText("PAUSE");
            lblTimer.setText("Timer Resumed AT "+timerStartedAt);
            updateTimer(false, false);
        }
    }


    public void handleTimerStop(View view) {
        updateTimer(false, true);
        Intent intent = new Intent(this, TimeSheet.class);
        startActivity(intent);
        finish();
    }


}
