package com.escoffier.myexternship;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class TrackTime extends AppCompatActivity {
    TimeSheetData mTsData;
    Date mDateShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_time);

        Spinner spinner = (Spinner) findViewById(R.id.spPrograms);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.arrPrograms, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        ContextWrapper cw = new ContextWrapper(this);
        mTsData = new TimeSheetData(cw);
        mDateShown = mTsData.getCurrentWeek();

        String dt = mTsData.getDataForDay(mDateShown, "today");
        double hrs = Double.parseDouble(mTsData.getDateData(dt));
        String tm = mTsData.getStringFromHrs(hrs);
        TextView  lblHoursToday = (TextView)findViewById(R.id.lblHoursToday);
        lblHoursToday.setText(tm);

        tm = mTsData.getTotalHours(mDateShown);
        TextView lblHoursWeek = (TextView)findViewById(R.id.lblHoursWeek);
        lblHoursWeek.setText(tm);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return  super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mnuSettings:
                handleSettings();
                return true;
            case R.id.mnuTimer:
                handleTimer();
                return true;
            case R.id.mnuTimeSheet:
                handleTimeSheet();
                return true;
            case R.id.mnuLogOff:
                handleLogOff();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void handleLogOff() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("email", "");
        editor.apply();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        finish();//we no longer need login
    }

    private void handleTimeSheet() {
        Intent intent = new Intent(this, TimeSheet.class);
        startActivity(intent);
    }

    private void handleTimer() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        long startTime =  sharedPref.getLong("timerStart", 0);

        Intent intent;

        if(startTime > 0){
            intent = new Intent(this, TrackTimeStop.class);
        }else{
            intent = new Intent(this, TrackTime.class);
        }

        startActivity(intent);
    }

    private void handleSettings() {
        Intent intent = new Intent(this, StudentProfile.class);
        startActivity(intent);
        finish();
    }

    public void handleTimerStart(View view) {
        SharedPreferences sharedPref = getSharedPreferences("com.escoffier.myexternship.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("timerStart", mTsData.getTime());
        editor.putString("timerStartedAt", mTsData.getFormatedTime());
        editor.apply();

        Intent intent = new Intent(this, TrackTimeStop.class);
        startActivity(intent);
        finish();
    }
}
