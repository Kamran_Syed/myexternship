package com.escoffier.myexternship;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Kamran on 4/23/2016.
 */
public class TimeSheetData {
    private Calendar mCalendar;
    private Date mCurrentWeek;
    private JSONObject mTimeData;
    private JSONObject mModifiedTimeData; //if you change put to both modified and timedata
    private JSONObject mWeekData;
    private JSONObject mModifiedWeekData;
    private Context mContext;


    public TimeSheetData(Context context){
        mCalendar = new GregorianCalendar(Locale.UK);
        Date today = new Date();
        mCalendar.setTime(today);
        mCalendar.set(Calendar.DAY_OF_WEEK, mCalendar.getFirstDayOfWeek());
        mCurrentWeek = mCalendar.getTime();
        mContext = context;
        loadDataFromSp(context);
    }

    public  Date getCurrentWeek(){
        return mCurrentWeek;
    }

    public Date getPreviousWeek(Date dt){
        mCalendar.setTime(dt);
        mCalendar.add(Calendar.HOUR, -168);
        mCalendar.set(Calendar.DAY_OF_WEEK, mCalendar.getFirstDayOfWeek());
        Log.d("MyApp","Week Previous: " + mCalendar.get(Calendar.DATE));
        return mCalendar.getTime();

    }

    public Date getNextWeek(Date dt){
        mCalendar.setTime(dt);
        mCalendar.add(Calendar.HOUR, +168);
        mCalendar.set(Calendar.DAY_OF_WEEK, mCalendar.getFirstDayOfWeek());
        Log.d("MyApp","Week Next: " + mCalendar.get(Calendar.DATE));
        return mCalendar.getTime();
    }

    public String getStringFromHrs(double hrs){
        double totalMinutes = hrs * 60;
        int hr = (int)totalMinutes/60;

        int minutes = (int) Math.round(totalMinutes % 60);
        minutes = minutes < 0? minutes + 60 : minutes;

        return String.format("%02d:%02d", hr, minutes);
    }

    public String getTotalHours(Date date){
        double total = 0;
        String dt;
        String tm;
        double hrs;

        dt = getDataForDay(date, "mon");
        hrs = Double.parseDouble(getDateData(dt));
        total += hrs;

        dt = getDataForDay(date, "tue");
        hrs = Double.parseDouble(getDateData(dt));
        total += hrs;

        dt = getDataForDay(date, "wed");
        hrs = Double.parseDouble(getDateData(dt));
        total += hrs;

        dt = getDataForDay(date, "thu");
        hrs = Double.parseDouble(getDateData(dt));
        total += hrs;

        dt = getDataForDay(date, "fri");
        hrs = Double.parseDouble(getDateData(dt));
        total += hrs;

        dt = getDataForDay(date, "sat");
        hrs = Double.parseDouble(getDateData(dt));
        total += hrs;

        dt = getDataForDay(date, "sun");
        hrs = Double.parseDouble(getDateData(dt));
        total += hrs;

        tm = getStringFromHrs(total);
        return  tm;
    }

    public String getFormatedTime(){
        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss a");
        mCalendar = Calendar.getInstance();
        return format.format(mCalendar.getTime());
    }

    public long getTime(){
        mCalendar = Calendar.getInstance();
        return mCalendar.getTimeInMillis();
    }

    public String getDataForDay(Date dt, String day){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        switch (day){
            case "mon":
                mCalendar.setTime(dt);
                //mCalendar.add(Calendar.HOUR, +24);
                return format.format(mCalendar.getTime());
            case "tue":
                mCalendar.setTime(dt);
                mCalendar.add(Calendar.HOUR, +24);
                return format.format(mCalendar.getTime());
            case "wed":
                mCalendar.setTime(dt);
                mCalendar.add(Calendar.HOUR, +48);
                return format.format(mCalendar.getTime());
            case "thu":
                mCalendar.setTime(dt);
                mCalendar.add(Calendar.HOUR, +72);
                return format.format(mCalendar.getTime());
            case "fri":
                mCalendar.setTime(dt);
                mCalendar.add(Calendar.HOUR, +96);
                return format.format(mCalendar.getTime());
            case "sat":
                mCalendar.setTime(dt);
                mCalendar.add(Calendar.HOUR, +120);
                return format.format(mCalendar.getTime());
            case "sun":
                mCalendar.setTime(dt);
                mCalendar.add(Calendar.HOUR, +144);
                return format.format(mCalendar.getTime());
            case "today":
                mCalendar = Calendar.getInstance();
                return format.format(mCalendar.getTime());

        }
        //should not fll here
        Log.d("My Externship","Error in getDataForDay ");
        return format.format(mCalendar.getTime());
    }

    private void loadDataFromSp(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("com.escoffier.myexternship.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
        String sJson =  sharedPref.getString("sjsondata", "");
        if(sJson.isEmpty()){
            mTimeData = new JSONObject();
            mModifiedTimeData = new JSONObject();
            mWeekData = new JSONObject();
            mModifiedWeekData = new JSONObject();
            return;
        }
        try {
            JSONObject reader = new JSONObject(sJson);
            mTimeData = reader.getJSONObject("TimeData");
            mModifiedTimeData = reader.getJSONObject("ModifiedTimeData");
            mWeekData = reader.getJSONObject("WeekData");
            mModifiedWeekData = reader.getJSONObject("ModifiedWeekData");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getJSON(){
        SharedPreferences sharedPref = mContext.getSharedPreferences("com.escoffier.myexternship.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
        String sJson =  sharedPref.getString("sjsondata", "");

        try {
            JSONObject reader = new JSONObject(sJson);
            return reader;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void saveDataToSp(){
        JSONObject dtData = new JSONObject();
        try {
            dtData.put("TimeData", mTimeData);
            dtData.put("ModifiedTimeData", mModifiedTimeData);
            dtData.put("WeekData", mWeekData);
            dtData.put("ModifiedWeekData", mModifiedWeekData);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        SharedPreferences sharedPref = mContext.getSharedPreferences("com.escoffier.myexternship.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("sjsondata", dtData.toString());
        editor.apply();

    }

    public void setJSON(JSONObject obj){
        SharedPreferences sharedPref = mContext.getSharedPreferences("com.escoffier.myexternship.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("sjsondata", obj.toString());
        editor.apply();
    }

    /**
     * returns hours for given date as string
     * @param dt is string format is like 2016-05-31
     */
    public String getDateData(String dt){
        try {
            return mTimeData.getString(dt);

        } catch (JSONException e) {
            return "0";
        }
    }

    public String getWeekData(String dt, String k){
        JSONObject tmp;
        try {
            tmp = mWeekData.getJSONObject("week-" + dt);
            return tmp.getString(k);

        } catch (JSONException e) {
            return "0";
        }

    }

    public void setDateData(String dt, String hrs){
        try {
            mTimeData.put(dt, hrs);
            mModifiedTimeData.put(dt, hrs);
            saveDataToSp();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setWeekData(String dt, String k, String v){
        try {
            JSONObject tmp;

            try {
                tmp = mWeekData.getJSONObject("week-" + dt);
            }catch (Exception ex){
                tmp = new JSONObject();
            }
            tmp.put(k, v);
            mWeekData.put("week-"+dt, tmp);
            saveDataToSp();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
